//
//  SBAnimationChainObject.m
//  GraphsTest
//
//  Created by Wim Haanstra on 5/29/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import "SBAnimationChainObject.h"

@implementation SBAnimationChainObject

- (id) init
{
	self = [super init];
	if (self) {
		_codeBlock = nil;
        _finishedBlock = nil;
	}
	
	return self;
}

@end