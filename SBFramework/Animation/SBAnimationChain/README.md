Animation-Chain
=============

I created this Animation-Chain class, because I was fed up chaining all kinds of animations together. When you want 2 animation chained together in iOS, that is no problem. You could just use the UIView animation methods and while you are using codeblocks, you can chain 2. The problem is, I came accross a couple of situation where I wanted to chain more than just the two.

This was the time I started working on this Animation-Chain.

This short code example will show you how it works  

	- (void) viewDidAppear:(BOOL)animated  
	{
		UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0,210, 320, 30)];
		label.backgroundColor = [UIColor redColor];
		label.textColor = [UIColor whiteColor];
		label.text = @"Wheeeee";
		[self.view addSubview:label];
	
		SBAnimationChain* chain = [[SBAnimationChain alloc] init];
		[chain addAnimationBlock:^(void) {
			label.frame = CGRectOffset(label.frame, 0, 100);
		} finishedBlock:^(void) {} withAnimationCurve:UIViewAnimationCurveEaseIn];

		[chain addAnimationBlock:^(void) {
			label.frame = CGRectOffset(label.frame, 0, -200);
		} finishedBlock:^(void) { } withAnimationCurve:UIViewAnimationCurveEaseIn];

		[chain addAnimationBlock:^(void) {
			label.frame = CGRectOffset(label.frame, 0, 100);
		} finishedBlock:^(void) {
			label.text = @"Pffffewww";
		} withAnimationCurve:UIViewAnimationCurveEaseIn];

		[chain startAnimation];
	}

I chain 3 animations together, which move the label I just created, around a bit. When the 3rd animation stops, it changes the text on the label.

Properties
----------

`@property (nonatomic, retain) NSMutableArray* animationItems;`  
Contains the animation items encapsuled in SBAnimationChainObject types.

`@property (nonatomic, readonly) NSInteger	 currentAnimationIndex;`  
Contains the current index of the animation that is performing.

`@property (nonatomic, readonly) CGFloat currentAnimationBlockTime;`  
Contains the time (in float) each animation block takes.

`@property CGFloat totalAnimationTime;`  
With this property you can set how long the total animation takes.

Methods
-------
`- (void) addAnimationBlock:(void(^)(void)) codeBlock finishedBlock:(void(^)(void)) finishedBlock withAnimationCurve:(UIViewAnimationCurve) animationCurve`  
Add an animation/code block to the animation chain, you can also specify a finishedBlock which is performed after this specific animation is finished. The `animationCurve` is the UIViewAnimationCurve used for this animation.

`- (void) startAnimation`  
When you are done adding animations, call this to start the animation.

`- (void) startAnimationFromIndex:(int) index`  
Want to start an animation from a certain index, call this, the `totalAnimationTime` is shortened by the amount of animations skipped.

`- (void) stopAnimation`  
Calling this will stop the animationChain, but the currently running animation will continue till it is finished.

`- (void) resumeAnimation`  
Calling this will start the animationChain again from where you called `stopAnimation`.

`- (void) clearAnimation`  
Clears the animationChain, ready for reuse

