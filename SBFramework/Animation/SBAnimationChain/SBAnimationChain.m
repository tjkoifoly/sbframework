//
//  SBAnimationChain.m
//  GraphsTest
//
//  Created by Wim Haanstra on 5/28/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import "SBAnimationChain.h"

@implementation SBAnimationChain

- (id) init
{
	self = [super init];
	
	if (self) {
        [self setAnimationItems:[[NSMutableArray alloc] init]];
        [self setTotalAnimationTime:1.0f];
		stopAnimation = NO;
	}
	
	return self;
}

- (void) addAnimationBlock:(void(^)(void)) codeBlock finishedBlock:(void(^)(void)) finishedBlock withAnimationCurve:(UIViewAnimationCurve) animationCurve;
{
	SBAnimationChainObject* o = [[SBAnimationChainObject alloc] init];
    [o setCodeBlock:codeBlock];
    [o setFinishedBlock:finishedBlock];
    [o setAnimationCurve:animationCurve];
	[_animationItems addObject:o];
} 

- (void) startAnimation
{
	stopAnimation = NO;
	
	if ([_animationItems count] == 0)
		return;
	
	CGFloat animationTimePerBlock = [self totalAnimationTime] / (CGFloat)[_animationItems count];
	[self animateBlock:0 animationTime:animationTimePerBlock];
}

- (void) startAnimationFromIndex:(NSUInteger) index
{
	stopAnimation = NO;
	
	int animationsLeft = [_animationItems count] - index;
	
	CGFloat animationTimePerBlock = [self totalAnimationTime] / (CGFloat)animationsLeft;
	[self animateBlock:index animationTime:animationTimePerBlock];
}

- (void) stopAnimation
{
	stopAnimation = YES;
}

- (void) resumeAnimation
{
	[self animateBlock:_currentAnimationIndex animationTime:_currentAnimationBlockTime];
}

- (void) clearAnimation
{
	[_animationItems removeAllObjects];
}

- (void) animateBlock:(NSUInteger) index animationTime:(CGFloat) animationTime
{
	if (index >= [_animationItems count])
		return;
	
	_currentAnimationBlockTime = animationTime;
	_currentAnimationIndex = index;

	SBAnimationChainObject* o = [_animationItems objectAtIndex:index];
	
	[UIView animateWithDuration:animationTime 
						  delay:0.0f 
						options:UIViewAnimationOptionWithCurve(o.animationCurve) 
					 animations:[o codeBlock]
					 completion:^(BOOL finished) {
		
						 o.finishedBlock();
						 
						 if (stopAnimation == NO)
							 [self animateBlock:index + 1 animationTime:animationTime];
					 }
	 ];
	
}



@end
