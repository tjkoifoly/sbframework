[![Sorted Bits icon](http://www.sortedbits.com/wp-content/uploads/2012/06/sb-logo.png)](http://www.sortedbits.com)

#SBFramework

## Introduction
**SBFramework** is a framework, well more of a collection of classes, which I use in almost every project I do. It's not so much about making functionality not already in the iOS SDK, but more about making life easier.

I divided the framework in a couple of sections, each containing methods used for a certain purpose.

The framework uses ARC (where necessary) and is tested from iOS 5 to the latest beta release.

## Sections
* [Animation](#animation)
  * [SBAnimationChain](#animation-chain)
* [Extensions](#extensions)
  * [AnimationExtensions](#animationextensions)
  * [CGRectExtensions](#cgrectextensions)
  * [NSFileManager+Directories](#nsfilemanager-directories)
  * [UIViewController+Rotation](#uiviewcontroller-rotation)
* [Graphs](#graphs)
  * [SBStackedHorizontalBar](#sbstackedhorizontalbar)
* [Views](#views)
  * [BatteryView](#batteryview)

## <a name="#animation"></a>Animation
### <a name="#animation-chain"></a>SBAnimationChain
**SBAnimationChain** lets you chain animations together in an easy and intiuitive way. With this class you can chain together more than the normal 2 animations, which will be executed after eachother.
  
[Read more about it here](https://github.com/depl0y/SBFramework/tree/master/Animation/SBAnimationChain)

## <a name="#extensions"></a>Extensions

### <a name="#animationextensions"></a>AnimationExtensions
**These are all static inline functions. So they can be called without instantiation objects.**  
  
`UIViewAnimationOptions UIViewAnimationOptionWithCurve(UIViewAnimationCurve curve)`  
This method takes an `UIViewAnimationCurve` and returns a `UIViewAnimationOptions` object for it.

### <a name="#cgrectextensions"></a>CGRectExtensions
**These are all static inline functions. So they can be called without instantiation objects.**  
  
`CGRect CGRectMoveToPoint(CGRect rect, CGPoint point)`  
Move a `CGRect` to a new origin  

`CGRect CGRectResize(CGRect rect, CGFloat width, CGFloat height)`  
Resize a `CGRect` to have a new width and height  

`CGRect CGRectSwitchWidthAndHeight(CGRect rect)`  
Swap the width and height of a `CGRect`

`void CGRectLog(CGRect rect, NSString* description)`  
Logs a `CGRect` with an optional description. Output will be in the format `%@: %f x %f x %f x %f`, where the first parameter is the description and the following float parameters are the properties of the supplied `CGRect`.

### <a name="#nsfilemanager-directories"></a>NSFileManager+Directories  
**These methods are all extensions to the NSFileManager class.**  
  
`- (NSString*) directoryInDocumentsDirectory:(NSString*) path;`  
This method gets the *documents* directory for the iOS application, but it creates a folder named `path` in there and then returns the full path of that directory (including path to the documents directory).

`- (NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString*) extension;`  
This method finds all files with a certain `extension` in the supplied `directory`. It returns the filenames in an `NSArray`.

### <a name="#uiviewcontroller-rotation"></a>UIViewController+Rotation  
**These methods are all extensions to the UIViewCntroller class**  
  
`- (BOOL) shouldAllowRotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation;`  
This method checks the current `plist` for allowed orientations of the application. It also checks if you are running on an iPhone or an iPad and gets the appropriate values (if the iPad allowed orientations are defined). It returns `YES` when the orientation is allowed, otherwise `NO`.

## <a name="#graphs"></a>Graphs
### <a name="#sbstackedhorizontalbar"></a>SBStackedHorizontalBar
The **SBStackedHorizontalBar** allows you to create an iTunes like horizontal graph bar in iOS.  
  
[Read more about it here](https://github.com/depl0y/SBFramework/tree/master/Graphs/SBStackedHorizontalBar)

## <a name="#views"></a>Views
### <a name="#batteryview"></a>BatteryView
BatteryView is a simple `UIView` which shows the current battery status of the iOS device. BatteryLevel change notifications are registered and handled within the view itself, which means you just have to include it and place the view somewhere on your own `UIView`.  
  
Graphics for the **BatteryView** are also supplied in the framework, but can easily be overridden by using the `batteryImage` and the `loadImage` properties of the view.