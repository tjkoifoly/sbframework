//
//  SBAlertView.m
//  SBFramework
//
//  Created by Wim Haanstra on 6/19/12.
//
//

#import "SBAlertView.h"

@implementation SBAlertView

- (id) initWithTitle:(NSString*) __title message:(NSString*) __message
   cancelButtonTitle:(NSString*) __cancelButtonTitle
   cancelButtonBlock:(void(^)(void)) __cancelButtonBlock
{
    if (self == [super init])
	{
        self.title = __title;
        self.message = __message;
        self.cancelButtonTitle = __cancelButtonTitle;
        self.cancelButtonCodeBlock = __cancelButtonBlock;
        buttonBlocks = [[NSMutableDictionary alloc] init];
        
        
    }
	
	return self;
}

- (void) addButton:(NSString*) alertTitle block:(void(^)(void)) actionBlock
{
    [buttonBlocks setObject:actionBlock forKey:alertTitle];
}

- (void) show
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:_title message:_message delegate:self cancelButtonTitle:_cancelButtonTitle otherButtonTitles:nil];
    
    for (NSString* key in [buttonBlocks allKeys])
    {
        [alertView addButtonWithTitle:key];
    }
    
    [alertView show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString* buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"%@", buttonTitle);
}

/*
 UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Payment failed", @"Payment failed alert title") message:[NSString stringWithFormat:@"Sorry, but your payment failed with the following error:\n\n%@", errorMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alertView show];
 
 */
@end
