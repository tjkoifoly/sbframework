//
//  SBTextView.m
//  SBFramework
//
//  Created by Wim Haanstra on 1/18/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import "SBTextView.h"

@implementation SBTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		_maximumLines = 20;
		_maximumLineLength = 30;
		
		_textView = [[UITextView alloc] initWithFrame:CGRectMake(0,0,frame.size.width, frame.size.height)];
		_textView.backgroundColor = [UIColor clearColor];
		_textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_textView.delegate = self;
		
		_placeholder = [UILabel new];
		
		_placeholder.frame = CGRectMake(10, 0, _textView.frame.size.width - 20, 34);
		_placeholder.text = @"Your text here";
		_placeholder.backgroundColor = [UIColor clearColor];
		_placeholder.textColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void) setPlaceholderString:(NSString *) placeholderString {
	_placeholder.text = placeholderString;
}

- (NSString*) placeholderString {
	return _placeholder.text;
}

- (void) textViewDidBeginEditing:(UITextView *)textView {
	if ([_delegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
		return [_delegate textViewDidBeginEditing:textView];
	}
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	if (![_textView hasText]) {
		[_textView addSubview:_placeholder];
	}
	else {
		[_placeholder removeFromSuperview];
	}
	
	if ([_delegate respondsToSelector:@selector(textViewDidEndEditing:)]) {
		return [_delegate textViewDidEndEditing:textView];
	}
	
}

- (void)textViewDidChange:(UITextView *)textView {
	if (![_textView hasText]) {
		[_textView addSubview:_placeholder];
	}
	else {
		[_placeholder removeFromSuperview];
	}
		
	if ([_delegate respondsToSelector:@selector(textViewDidChange:)]) {
		return [_delegate textViewDidChange:textView];
	}
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
	if ([_delegate respondsToSelector:@selector(textViewDidChangeSelection:)]) {
		return [_delegate textViewDidChangeSelection:textView];
	}
	
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
	if ([_delegate respondsToSelector:@selector(textViewShouldBeginEditing:)]) {
		return [_delegate textViewShouldBeginEditing:textView];
	}
	else
		return YES;
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView {
	if ([_delegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
		return [_delegate textViewShouldEndEditing:textView];
	}
	else
		return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	
	BOOL response =	[self textViewShouldChange:_textView textChangeRange:range text:text];
	if ([_delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
		return [_delegate textView:textView shouldChangeTextInRange:range replacementText:text];
	}
	else
		return response;
}



// Perform a simple word-wrap
- (BOOL) textViewShouldChange:(UITextView*) textView textChangeRange:(NSRange) range text:(NSString*) text {
	
	NSArray* lines = [textView.text componentsSeparatedByString:@"\n"];
	NSCharacterSet* charSet = [NSCharacterSet characterSetWithCharactersInString:@" .-/,;:"];
	
	// User backspaced, check current line length
	if ([text length] == 0 && ![text isEqualToString:@"\n"]) {
		if (range.location == 0 && [textView.text length] == 0) {
			NSLog(@"(range.location == 0 && [textView.text length] == 0)");
			return NO;
		}
		else if (range.location > 0 && ![[textView.text substringWithRange:NSMakeRange(range.location, 1)] isEqualToString:@"\n"]) {
			NSLog(@"(range.location > 0 && ![[textView.text substringWithRange:NSMakeRange(range.location - 1, 1)] isEqualToString:NewLine])");
			return YES;
		}
		else if (range.location == 0 && ![[textView.text substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"\n"]) {
			NSLog(@"(range.location == 0 && ![[textView.text substringWithRange:NSMakeRange(0, 1)] isEqualToString:NewLine])");
			return YES;
		}
		else if (range.location > 0 && [[textView.text substringWithRange:NSMakeRange(range.location, 1)] isEqualToString:@"\n"]) {
			NSLog(@"(range.location > 0 && [[textView.text substringWithRange:NSMakeRange(range.location, 1)] isEqualToString:NewLine])");
			
			// Store altered text in a temp string, for validation
			NSString* tempText = [textView.text stringByReplacingCharactersInRange:range withString:@""];
			
			// Now get the current line length
			NSRange beforeRangeBreak = [tempText rangeOfString:@"\n" options:NSBackwardsSearch range:NSMakeRange(0, range.location)];
			NSRange afterRangeBreak = [tempText rangeOfString:@"\n" options:0 range:NSMakeRange(range.location, [tempText length] - range.location)];
			// We are on the first line, just appending text. No problem if the line is short enough
			int lineLength = 0;
			NSRange lineRange;
			if (beforeRangeBreak.location == NSNotFound && afterRangeBreak.location == NSNotFound) {
				lineLength = [tempText length];
				
				lineRange = NSMakeRange(0, lineLength);
			}
			else if (beforeRangeBreak.location == NSNotFound && afterRangeBreak.location != NSNotFound) {
				lineLength = afterRangeBreak.location;
				lineRange = NSMakeRange(0, lineLength);
			}
			else if (beforeRangeBreak.location != NSNotFound && afterRangeBreak.location == NSNotFound) {
				lineLength = [tempText length] - beforeRangeBreak.location;
				lineRange = NSMakeRange(beforeRangeBreak.location, lineLength);
			}
			else if (beforeRangeBreak.location != NSNotFound && afterRangeBreak.location != NSNotFound) {
				lineLength = afterRangeBreak.location - beforeRangeBreak.location;
				lineRange = NSMakeRange(beforeRangeBreak.location, lineLength);
			}
			
			// After removing a new line, check if the line is longer than
			// the max defined line
			NSLog(@"New line length: %d", lineLength);
			
			if (lineLength >= _maximumLineLength) {
				NSLog(@"Line to long!");
				// Find last space in the current line
				//				NSRange lastSpaceInLine = [tempText rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(lineRange.location, _maximumLineLength)];
				NSRange lastSpaceInLine = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(lineRange.location, _maximumLineLength)];
				if (lastSpaceInLine.location != NSNotFound) {
					NSLog(@"Last space found");
					tempText = [tempText stringByReplacingCharactersInRange:NSMakeRange(lastSpaceInLine.location, 1) withString:@"\n"];
				}
				else {
					// Not sure what to do when there are no spaces in the line and you are backspacing.
				}
			}
			
			textView.text = tempText;
			[textView setSelectedRange:NSMakeRange(range.location, 0)];
			return NO;
		}
		else
			return NO;
	}
	// This is for entering text, instead of backspacing
	else if ([text length] > 0 && ![text isEqualToString:@"\n"]) {
		
		BOOL isLastCharacter = NO;
		if (range.location == [textView.text length])
			isLastCharacter = YES;
		
		// Should get current line where the text is edited
		NSRange beforeRangeBreak = [textView.text rangeOfString:@"\n" options:NSBackwardsSearch range:NSMakeRange(0, range.location)];
		NSRange afterRangeBreak = [textView.text rangeOfString:@"\n" options:0 range:NSMakeRange(range.location, [textView.text length] - range.location)];
		// We are on the first line, just appending text. No problem if the line is short enough
		if (beforeRangeBreak.location == NSNotFound && range.location < _maximumLineLength && isLastCharacter) {
			return YES;
		}
		// We are on the first line, inserting text, but there is no new line in this text yet
		else if (beforeRangeBreak.location == NSNotFound && afterRangeBreak.location == NSNotFound) {
			NSLog(@"(beforeRangeBreak.location == NSNotFound && afterRangeBreak.location == NSNotFound)");
			
			// We are at the end of the first line, break break break
			if ([[textView text] length] >= _maximumLineLength) {
				
				if ([lines count] == _maximumLines)
					return NO;
				
				//				NSRange lastSpace = [textView.text rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(0, [textView.text length])];
				NSRange lastSpace = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(0, [textView.text length])];
				
				// No space found, we should just break here, IF we aren't at the max rows count!
				if (lastSpace.location == NSNotFound) {
					textView.text = [textView.text stringByAppendingString:@"\n"];
				}
				else if (lastSpace.location != NSNotFound) {
					textView.text = [textView.text stringByReplacingCharactersInRange:lastSpace withString:@"\n"];
				}
				
				return YES;
			}
			// The line is not long enough to break yet.
			else if ([textView.text length] < _maximumLineLength) {
				return YES;
			}
		}
		// We are in the first line, but we are inserting text and a newline exists
		else if (beforeRangeBreak.location == NSNotFound && afterRangeBreak.location != NSNotFound && !isLastCharacter) {
			NSLog(@"(beforeRangeBreak.location == NSNotFound && afterRangeBreak.location != NSNotFound && !isLastCharacter)");
			
			int lineLength = 0;
			lineLength = afterRangeBreak.location;
			
			if (lineLength >= _maximumLineLength) {
				if ([lines count] == _maximumLines)
					return NO;
				
				//NSRange lastSpace = [textView.text rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(0, lineLength)];
				NSRange lastSpace = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(0, lineLength)];
				
				if (lastSpace.location != NSNotFound) {
					textView.text = [textView.text stringByReplacingCharactersInRange:lastSpace withString:@"\n"];
				}
				else {
					textView.text = [textView.text stringByReplacingCharactersInRange:NSMakeRange(_maximumLineLength, 1) withString:@"\n"];
				}
				
				[textView setSelectedRange:range];
				
				return YES;
			}
			else if (lineLength < _maximumLineLength)
				return YES;
		}
		// We are inserting text in the textview, there are linebreaks before
		// and after the place we are inserting it.
		else if (beforeRangeBreak.location != NSNotFound && afterRangeBreak.location != NSNotFound) {
			NSLog(@"(beforeRangeBreak.location != NSNotFound && afterRangeBreak.location != NSNotFound)");
			
			int lineLength = afterRangeBreak.location - beforeRangeBreak.location;
			NSLog(@"Line length: %d", lineLength);
			
			if (lineLength < _maximumLineLength)
				return YES;
			else if (lineLength >= _maximumLineLength) {
				if ([lines count] == _maximumLines) {
					return NO;
				}
				
				NSLog(@"Range: %d - %d", beforeRangeBreak.location, afterRangeBreak.location);
				
				//				NSRange lastSpace = [textView.text rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, afterRangeBreak.location - beforeRangeBreak.location)];
				NSRange lastSpace = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, afterRangeBreak.location - beforeRangeBreak.location)];
				
				if (lastSpace.location != NSNotFound) {
					textView.text = [textView.text stringByReplacingCharactersInRange:lastSpace withString:@"\n"];
				}
				else {
					textView.text = [textView.text stringByReplacingCharactersInRange:NSMakeRange(range.location, 0) withString:@"\n"];
				}
				
				[textView setSelectedRange:range];
				
			}
		}
		// We are appending text to the textview, there are linebreaks and
		// the inserted character is the last character in the text view
		else if (beforeRangeBreak.location != NSNotFound && afterRangeBreak.location == NSNotFound && isLastCharacter) {
			NSLog(@"(beforeRangeBreak.location != NSNotFound && afterRangeBreak.location == NSNotFound && isLastCharacter)");
			int lineLength = [textView.text length] - beforeRangeBreak.location;
			
			NSLog(@"Line length: %d", lineLength);
			if (lineLength < _maximumLineLength)
				return YES;
			else if (lineLength >= _maximumLineLength) {
				if ([lines count] == _maximumLines) {
					return NO;
				}
				
				//				NSRange lastSpace = [textView.text rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, lineLength)];
				NSRange lastSpace = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, lineLength)];
				
				if (lastSpace.location != NSNotFound) {
					textView.text = [textView.text stringByReplacingCharactersInRange:lastSpace withString:@"\n"];
				}
				else {
					textView.text = [textView.text stringByReplacingCharactersInRange:NSMakeRange(_maximumLineLength, 1) withString:@"\n"];
				}
				
				[textView setSelectedRange:range];
				
				return YES;
				
			}
		}
		// We are inserting text on the last line in the text view
		else if (beforeRangeBreak.location != NSNotFound && afterRangeBreak.location == NSNotFound && !isLastCharacter) {
			NSLog(@"(beforeRangeBreak.location != NSNotFound && afterRangeBreak.location == NSNotFound && !isLastCharacter)");
			
			int lineLength = [textView.text length] - beforeRangeBreak.location;
			
			if (lineLength >= _maximumLineLength) {
				if ([lines count] == _maximumLines)
					return NO;
				
				//NSRange lastSpace = [textView.text rangeOfString:@" " options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, lineLength)];
				NSRange lastSpace = [textView.text rangeOfCharacterFromSet:charSet options:NSBackwardsSearch range:NSMakeRange(beforeRangeBreak.location, lineLength)];
				
				if (lastSpace.location != NSNotFound) {
					textView.text = [textView.text stringByReplacingCharactersInRange:lastSpace withString:@"\n"];
				}
				else {
					textView.text = [textView.text stringByReplacingCharactersInRange:NSMakeRange(beforeRangeBreak.location + lineLength, 1) withString:@"\n"];
				}
				
				[textView setSelectedRange:range];
				
				return YES;
			}
			else if (lineLength < _maximumLineLength)
				return YES;
		}
	}
	else if ([text isEqualToString:@"\n"]) {
		if ([lines count] == _maximumLines) {
			return NO;
		}
	}
	
	return YES;
}


@end
