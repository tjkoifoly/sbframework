//
//  SBPopover.m
//  WorkPlace
//
//  Created by Wim Haanstra on 1/9/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import "SBPopover.h"
#import "CGRectExtensions.h"
#import "UIApplication+ScreenSize.h"
#import <QuartzCore/QuartzCore.h>

@implementation SBPopover

- (id) init {
	self = [super init];
	
	if (self) {
		
		self.backgroundColor = [UIColor blackColor];
		
		_titleView = nil;
		_dimView = [[UIView alloc] initWithFrame:CGRectZero];
		_menuItemViews = [NSArray new];
		_contentView = [[UIView alloc] initWithFrame:CGRectZero];
		_configuration = [SBPopoverConfiguration defaultOptions];
		_contentView.backgroundColor = [UIColor blackColor];
		_contentView.layer.cornerRadius = 4.0;
		_contentView.layer.shadowColor = [UIColor blackColor].CGColor;
		_contentView.layer.shadowOffset = CGSizeMake(0, 0);
		_contentView.layer.shadowOpacity = 0.5f;
		_contentView.layer.shadowRadius = 5;
	}
	
	return self;
}

/* Show popover at the center of the screen */
- (void) show {
	[_dimView removeFromSuperview];
	[self setupContentView];
	
	_contentView.alpha = 0.0f;
	_contentView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
	
	self.backgroundColor = [UIColor clearColor];
	
	if (self.configuration.darkenBackground) {
		_dimView.alpha = 0.6;
		_dimView.backgroundColor = [UIColor blackColor];
		[self addSubview:_dimView];
	}
	_contentView.backgroundColor = [UIColor redColor];
	
	UIView* topView = [[UIApplication sharedApplication] topView];
	
	self.frame = CGRectZeroLocation(topView.frame);
	
	_dimView.frame = self.frame;
	_contentView.frame = CGRectCenterInRect(self.frame, CGSizeMake(_contentView.frame.size.width, _contentView.frame.size.height));
	
	[self addSubview:_contentView];
	
	CGRectLogWithDescription(@"ContentView", _contentView.frame);
	CGRectLogWithDescription(@"Frame", self.frame);

	
	[topView addSubview:self];
	
	[UIView animateWithDuration:0.2
						  delay:0
						options:UIViewAnimationCurveEaseInOut
					 animations:^(void) {
						 _contentView.alpha = 1.0f;
						 _contentView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
					 }
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.08f
											   delay:0
											 options:UIViewAnimationCurveEaseInOut
										  animations:^(void) {
											  _contentView.transform = CGAffineTransformIdentity;
										  }
										  completion:^(BOOL finished) {
										  }
						  ];
					 }
	 ];
}

//- (CGPoint) determine

/* Show the popover with an arrow from a rectangle. It will display an arrow
 pointing to this rectangle.*/
- (void) showFromRect:(CGRect) rect {
	
}

/* Show the popover with an arrow from a point. It will display an arrow
 pointing to this point.*/
- (void) showFromPoint:(CGPoint) point {
	
}

/* Show popover from a specific UIBarButtonItem */
- (void) showFromUIBarButtonItem:(UIBarButtonItem*) item {
	
}

- (CGPoint) determineArrowPoint {
	UIView* topView = [[UIApplication sharedApplication] topView];
	
	
	return CGPointZero;
}

/* Hide the Popover */
- (void) hide {
	[UIView animateWithDuration:0.08f
						  delay:0
						options:UIViewAnimationCurveEaseInOut
					 animations:^(void) {
						 _contentView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
					 }
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.2f
											   delay:0
											 options:UIViewAnimationCurveEaseInOut
										  animations:^(void) {
											  _contentView.alpha = 0.0f;
											  _contentView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
										  }
										  completion:^(BOOL finished) {
											  [self removeFromSuperview];
											  _contentView.transform = CGAffineTransformIdentity;
										  }
						  ];
					 }
	 ];
}

- (void) setupContentView {
	
	CGFloat totalHeight = self.configuration.padding;
	
	if (_titleView != nil) {
		_titleView.frame = CGRectMoveToPoint(_titleView.frame, CGPointMake(self.configuration.padding, totalHeight));
		[_contentView addSubview:_titleView];
		totalHeight += _titleView.frame.size.height;
	}
	
	totalHeight += self.configuration.padding;
	
	for (UIView* v in self.menuItemViews) {
		v.frame = CGRectMoveToPoint(v.frame, CGPointMake(self.configuration.padding, totalHeight));
		[_contentView addSubview:v];
		totalHeight += v.frame.size.height + self.configuration.padding;
	}

	_contentView.frame = CGRectMake(0, 0, self.configuration.maximumWidth, totalHeight);
	[self addSubview:_contentView];
}

/* The title string to show at the top of the Popover menu, this is a
 shortcut to quickly create a UIView and it will be placed in the titleView
 property. This will be overwritten when you use the titleView property */
- (void) setTitle:(NSString *) title {
	
	CGSize titleSize = [title sizeWithFont:self.configuration.titleFont constrainedToSize:CGSizeMake(self.configuration.maximumWidth - (self.configuration.padding * 2), 999) lineBreakMode:NSLineBreakByWordWrapping];
	
	if (titleSize.height < self.configuration.titleHeight)
		titleSize = CGSizeMake(titleSize.width, self.configuration.titleHeight);
	
	if (titleSize.width < self.configuration.maximumWidth - (self.configuration.padding * 2))
		titleSize = CGSizeMake(self.configuration.maximumWidth - (self.configuration.padding * 2), titleSize.height);
	
	UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.configuration.padding,
																	self.configuration.padding,
																	titleSize.width,
																	titleSize.height)];

	titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
	titleLabel.numberOfLines = 0;
	titleLabel.textAlignment = self.configuration.titleTextAlignment;
	titleLabel.backgroundColor = [UIColor clearColor];
	titleLabel.font = self.configuration.titleFont;
	titleLabel.textColor = self.configuration.titleFontColor;
	titleLabel.text = title;
	
	_titleView = titleLabel;
}

/* Shortcut to adding a MenuItemView, this is converted to an UIView
 and then added to the menuItemViews array */
- (void) addMenuItem:(NSString*) itemName tag:(NSInteger) tag {

	CGSize itemSize = [itemName sizeWithFont:self.configuration.menuItemFont constrainedToSize:CGSizeMake(self.configuration.maximumWidth - (self.configuration.padding * 2), 999) lineBreakMode:NSLineBreakByWordWrapping];
	
	if (itemSize.height < self.configuration.menuItemHeight)
		itemSize = CGSizeMake(itemSize.width, self.configuration.menuItemHeight);
	
	if (itemSize.width < self.configuration.maximumWidth - (self.configuration.padding * 2))
		itemSize = CGSizeMake(self.configuration.maximumWidth - (self.configuration.padding * 2), itemSize.height);

	UIButton* itemButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, itemSize.width, itemSize.height)];
	
	[itemButton setTitleColor:self.configuration.menuItemFontColor forState:UIControlStateNormal];
	
	itemButton.backgroundColor = [UIColor clearColor];
	itemButton.titleLabel.font = self.configuration.menuItemFont;
	itemButton.contentHorizontalAlignment = self.configuration.menuItemTextAlignment;
	itemButton.tag = tag;

	[itemButton setTitle:itemName forState:UIControlStateNormal];
	[itemButton addTarget:self action:@selector(menuItemClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	NSMutableArray* tempArray = [NSMutableArray arrayWithArray:_menuItemViews];
	[tempArray addObject:itemButton];
	_menuItemViews = [NSArray arrayWithArray:tempArray];
}

/* You can add any view to the menuItemsView array here.
 The tag is used to identify which menu item is clicked */
- (void) addMenuItemView:(UIView*) view {
	
	NSMutableArray* tempArray = [NSMutableArray arrayWithArray:_menuItemViews];
	[tempArray addObject:view];
	_menuItemViews = [NSArray arrayWithArray:tempArray];
	
}

- (void) menuItemClicked:(UIButton*) button {
	if ([_delegate respondsToSelector:@selector(popover:didSelectItemWithTag:)])
		[_delegate popover:self didSelectItemWithTag:button.tag];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if (self.configuration.closeOnClickOutside)
		[self hide];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
	/*
	NSString* test = @"test";
	
	[test drawAtPoint:CGPointMake(0,0) withFont:self.configuration.titleFont];
	*/
	
	
    // Drawing code
}


@end
