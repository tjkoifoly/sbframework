//
//  SBPopoverConfiguration.h
//  WorkPlace
//
//  Created by Wim Haanstra on 1/10/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBPopoverConfiguration : NSObject

#pragma mark - Generic options

@property (nonatomic) CGFloat padding;

@property (nonatomic) BOOL darkenBackground;

@property (nonatomic) BOOL closeOnClickOutside;

#pragma mark - Arrow Configuration Options

@property (nonatomic) CGFloat arrowHeight;

#pragma mark - Title View Configuration Options

/* Font used for displaying the title */
@property (nonatomic, retain) UIFont* titleFont;

/* Color used for displaying the title */
@property (nonatomic, retain) UIColor* titleFontColor;

/* The height of the title view */
@property (nonatomic) CGFloat titleHeight;

@property (nonatomic) NSTextAlignment titleTextAlignment;

#pragma mark - Menu Item View Configuration Options

/* Font used for displaying the menu item */
@property (nonatomic, retain) UIFont* menuItemFont;

/* Color used for displaying the menu item text */
@property (nonatomic, retain) UIColor* menuItemFontColor;

/* Menu item height */
@property (nonatomic) CGFloat menuItemHeight;

@property (nonatomic) UIControlContentHorizontalAlignment menuItemTextAlignment;

/* Maximum width of the popover */
@property (nonatomic) CGFloat maximumWidth;

@property (nonatomic) BOOL showDividerBetweenViews;

/* Load the default values for the configuration */
+ (SBPopoverConfiguration*) defaultOptions;

@end
