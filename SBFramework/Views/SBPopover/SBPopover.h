//
//  SBPopover.h
//  WorkPlace
//
//  Created by Wim Haanstra on 1/9/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBPopoverConfiguration.h"

@class SBPopover;

@protocol SBPopoverDelegate

@optional
- (void) popover:(SBPopover*) popover didSelectItemWithTag:(NSInteger) tag;
- (void) popoverDidDismiss:(SBPopover *)popoverView;

@end

@interface SBPopover : UIView {
	UIView* _dimView;
}

@property (nonatomic, assign) id<NSObject, SBPopoverDelegate> delegate;

/* The title view to show at the top of the Popover menu
 This will be overwritten when you use the title property */
@property (nonatomic, retain) UIView* titleView;

/* The views of the menu items to be displayed */
@property (nonatomic, retain) NSArray* menuItemViews;

/* The view that is used to display menu items, it can also be replaced with your own view */
@property (nonatomic, retain) UIView* contentView;

/* By default the SBPopoverConfiguration will contain the default configuration,
 you can edit this class, override it or just edit the values on run time */
@property (nonatomic, retain) SBPopoverConfiguration* configuration;

/* Show popover at the center of the screen */
- (void) show;

/* Show the popover with an arrow from a rectangle. It will display an arrow 
 pointing to this rectangle.*/
- (void) showFromRect:(CGRect) rect;

/* Show the popover with an arrow from a point. It will display an arrow 
 pointing to this point.*/
- (void) showFromPoint:(CGPoint) point;

/* Show popover from a specific UIBarButtonItem */
- (void) showFromUIBarButtonItem:(UIBarButtonItem*) item;

/* Hide the Popover */
- (void) hide;

/* The title string to show at the top of the Popover menu, this is a
 shortcut to quickly create a UIView and it will be placed in the titleView
 property. This will be overwritten when you use the titleView property */
- (void) setTitle:(NSString *) title;

/* Shortcut to adding a MenuItemView, this is converted to an UIView
 and then added to the menuItemViews array */
- (void) addMenuItem:(NSString*) itemName tag:(NSInteger) tag;

/* You can add any view to the menuItemsView array here.
 The tag is used to identify which menu item is clicked */
- (void) addMenuItemView:(UIView*) view;

@end
