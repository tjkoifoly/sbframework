//
//  CGRectExtensions.m
//  postercreator
//
//  Created by Wim Haanstra on 1/7/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import "CGRectExtensions.h"

void CGRectLogWithDescription(NSString* description, CGRect rect) {
	NSLog(@"%@: %f x %f x %f x %f", description, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

void CGRectLog(CGRect rect) {
	NSLog(@"%f x %f x %f x %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

CGPoint CGRectCenter(CGRect rect) {
	return CGPointMake(CGRectGetMinX(rect) + CGRectGetWidth(rect) / 2, CGRectGetMinY(rect) + CGRectGetHeight(rect) / 2);
}

CGRect CGRectMoveToPoint(CGRect rect, CGPoint point) {
	return CGRectMake(point.x, point.y, rect.size.width, rect.size.height);
}

CGRect CGRectCenterInRect(CGRect rect, CGSize size) {
	return CGRectMake((rect.size.width / 2) - (size.width / 2), (rect.size.height / 2) - (size.height / 2), size.width, size.height);
}

CGRect CGRectSwitchSides(CGRect rect) {
	return CGRectMake(rect.origin.x, rect.origin.y, rect.size.height, rect.size.width);
}

void CGRectToRoundedCornersPath(CGContextRef context, CGRect rect, float cornerRadius) {
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + cornerRadius);
    CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - cornerRadius);
    CGContextAddArc(context, rect.origin.x + cornerRadius, rect.origin.y + rect.size.height - cornerRadius,
                    cornerRadius, M_PI, M_PI / 2, 1); //STS fixed
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - cornerRadius,
                            rect.origin.y + rect.size.height);
    CGContextAddArc(context, rect.origin.x + rect.size.width - cornerRadius,
                    rect.origin.y + rect.size.height - cornerRadius, cornerRadius, M_PI / 2, 0.0f, 1);
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + cornerRadius);
    CGContextAddArc(context, rect.origin.x + rect.size.width - cornerRadius, rect.origin.y + cornerRadius,
                    cornerRadius, 0.0f, -M_PI / 2, 1);
    CGContextAddLineToPoint(context, rect.origin.x + cornerRadius, rect.origin.y);
    CGContextAddArc(context, rect.origin.x + cornerRadius, rect.origin.y + cornerRadius, cornerRadius,
                    -M_PI / 2, M_PI, 1);
}

UIBezierPath* CGRectCreateRoundedCornersPath(CGRect rect, float cornerRadius) {
	
	
	UIBezierPath *popoverPath = [UIBezierPath bezierPath];
	/*
    [popoverPath moveToPoint:CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect) + cornerRadius)];//LT1
    [popoverPath addCurveToPoint:CGPointMake(xMin + cornerRadius, yMin) controlPoint1:CGPointMake(xMin, yMin + radius - cpOffset) controlPoint2:CGPointMake(xMin + radius - cpOffset, yMin)];//LT2
    
    //If the popover is positioned below (!above) the arrowPoint, then we know that the arrow must be on the top of the popover.
    //In this case, the arrow is located between LT2 and RT1
    if (!above) {
        [popoverPath addLineToPoint:CGPointMake(arrowPoint.x - kArrowHeight, yMin)];//left side
        [popoverPath addCurveToPoint:arrowPoint controlPoint1:CGPointMake(arrowPoint.x - kArrowHeight + kArrowCurvature, yMin) controlPoint2:arrowPoint];//actual arrow point
        [popoverPath addCurveToPoint:CGPointMake(arrowPoint.x + kArrowHeight, yMin) controlPoint1:arrowPoint controlPoint2:CGPointMake(arrowPoint.x + kArrowHeight - kArrowCurvature, yMin)];//right side
    }
    
    [popoverPath addLineToPoint:CGPointMake(xMax - cornerRadius, yMin)];//RT1
    [popoverPath addCurveToPoint:CGPointMake(xMax, yMin + radius) controlPoint1:CGPointMake(xMax - cornerRadius + cpOffset, yMin) controlPoint2:CGPointMake(xMax, yMin + cornerRadius - cpOffset)];//RT2
    [popoverPath addLineToPoint:CGPointMake(xMax, yMax - cornerRadius)];//RB1
    [popoverPath addCurveToPoint:CGPointMake(xMax - cornerRadius, yMax) controlPoint1:CGPointMake(xMax, yMax - radius + cpOffset) controlPoint2:CGPointMake(xMax - cornerRadius + cpOffset, yMax)];//RB2
    
    //If the popover is positioned above the arrowPoint, then we know that the arrow must be on the bottom of the popover.
    //In this case, the arrow is located somewhere between LB1 and RB2
    if (above) {
        [popoverPath addLineToPoint:CGPointMake(arrowPoint.x + kArrowHeight, yMax)];//right side
        [popoverPath addCurveToPoint:arrowPoint controlPoint1:CGPointMake(arrowPoint.x + kArrowHeight - kArrowCurvature, yMax) controlPoint2:arrowPoint];//arrow point
        [popoverPath addCurveToPoint:CGPointMake(arrowPoint.x - kArrowHeight, yMax) controlPoint1:arrowPoint controlPoint2:CGPointMake(arrowPoint.x - kArrowHeight + kArrowCurvature, yMax)];
    }
    
    [popoverPath addLineToPoint:CGPointMake(xMin + cornerRadius, yMax)];//LB1
    [popoverPath addCurveToPoint:CGPointMake(xMin, yMax - radius) controlPoint1:CGPointMake(xMin + radius - cpOffset, yMax) controlPoint2:CGPointMake(xMin, yMax - cornerRadius + cpOffset)];//LB2
    [popoverPath closePath];
	 */
	return popoverPath;
}

CGRect CGRectZeroLocation(CGRect rect) {
	return CGRectMake(0, 0, rect.size.width, rect.size.height);
}

CGPoint CGRectCenterOfSide(CGRect rect, CGRectSide side) {
	
	CGFloat x = 0;
	CGFloat y = 0;
	
	switch (side) {
		default:
		case CGRectSideBottom: {
			x = rect.origin.x + (rect.size.width / 2);
			y = rect.origin.y + rect.size.height;
			break;
		}
	}
	
	return CGPointMake(x, y);
}