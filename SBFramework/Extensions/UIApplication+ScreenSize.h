//
//  UIApplication+ScreenSize.h
//  WorkPlace
//
//  Created by Wim Haanstra on 1/10/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIApplication (ScreenSize)

- (CGSize) screenSize;
- (UIView*) topView;

@end
