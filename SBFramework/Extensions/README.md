Extensions
==========

AnimationExtensions
----------
`static inline UIViewAnimationOptions UIViewAnimationOptionWithCurve(UIViewAnimationCurve curve)`

It converts the `UIViewAnimationCurve` to a `UIViewAnimationOptions` struct.

CGRectExtensions
----------

`void CGRectLog(CGRect rect);`

Writes the origin and size of the `CGRect` using a `NSLog` in the format: `X x Y x Width x Height`.

`CGPoint CGRectCenter(CGRect rect);`

Returns the center of a `CGRect` as a `CGPoint`.

`CGRect CGRectCenterInRect(CGRect rect, CGSize size);`

Creates a `CGRect` with the specified size in the center of the specified `CGRect`.

`CGRect CGRectSwitchSides(CGRect rect);`

Switches the Width and Height of a `CGRect`.

`void CGRectToRoundedCornersPath(CGContextRef context, CGRect rect, float cornerRadius);`

Create a rectangle with rounded corners in the specified `CGContextRef`, using the `cornerRadius` parameter for the corners.

`CGRect CGRectZeroLocation(CGRect rect);`

Moves the specified `CGRect` to the position `{0,0}`.


NSFileManager+Directories
-------

`(NSString*) directoryInDocumentsDirectory:(NSString*) path;`

Returns the full path to a directory with the name `path` in the documents directory, when the path does not exist, it creates the path.

`(NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString*) extension;`

Find files in a specified directory with a specific extension

`(NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString *)extension andPattern:(NSString*) pattern;`

Find files in a specified directory with a specific extension and the file name contains the supplied pattern

`(NSUInteger) cleanDirectory:(NSString*) path filesWithExtension:(NSString*) extension;`

Remove files with a certain extension from the supplied path

UIToolbar+ButtonLocation
-------
`- (UIButton*) buttonForUIBarButtonItem:(UIBarButtonItem*) item;`

This finds the button in a `UIToolbar` for the specified `UIBarButtonItem`. If the `item` can't be found, it will return `nil`.

`- (CGRect) frameOfUIBarButtonItem:(UIBarButtonItem*) item;`

This will return the frame of the specified `UIBarButtonItem`. If the `item` can't be found, it will return `CGRectZero`.

UIViewController+Rotation
-------
This is a class extending the UIViewController. The method checks the project `Plist` which orientations are allowed and returns `YES` or `NO` accordingly.

`(BOOL) shouldAllowRotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation;`