//
//  NSFileManager+Directories.m
//  SBFramework
//
//  Created by Wim Haanstra on 6/15/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//
//

#import "NSFileManager+Directories.h"

@implementation NSFileManager (Directories)

- (NSString*) directoryInDocumentsDirectory:(NSString*) path
{
	NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* directoryPath = [[documentDirectories objectAtIndex:0] stringByAppendingPathComponent:path];
	
    BOOL isDir = NO;
    
	if (![self fileExistsAtPath:directoryPath isDirectory:&isDir])
	{
        isDir = YES;
        NSError* error = nil;
        BOOL result = [self createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (result)
            return directoryPath;
        else
        {
            NSLog(@"directoryInDocumentsDirectory:%@ failed: %@", path, [error description]);
            return nil;
        }
	}
    
    if (isDir)
        return directoryPath;
    else
        return nil;
}

- (NSUInteger) cleanDirectory:(NSString*) path filesWithExtension:(NSString*) extension
{
	NSUInteger filesRemoved = 0;
	
	NSArray* files = [self findFilesInDirectory:path withExtension:extension];
	NSLog(@"%d files found for removal", [files count]);
	for (NSString* file in files)
	{
		NSString* fullPath = [path stringByAppendingPathComponent:file];

		NSError* error = nil;
		[self removeItemAtPath:fullPath error:&error];
		if (error == nil)
			filesRemoved++;
	}
	
	return filesRemoved;
}

- (NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString *)extension andPattern:(NSString*) pattern {
    NSError* error = nil;
    
	NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error != nil)
    {
        NSLog(@"findFilesInDirectory:%@:%@ failed: %@", directory, extension, [error description]);
        return [NSArray array];
    }
    
	NSMutableArray* returnValue = [[NSMutableArray alloc] init];
	for (NSString* file in files)
	{
		if ([[file pathExtension] isEqualToString:extension] && [file rangeOfString:pattern].location != NSNotFound)
			[returnValue addObject:file];
	}
	
	return [NSArray arrayWithArray:returnValue];
}

- (NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString*) extension
{
    NSError* error = nil;
    
	NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error != nil)
    {
        NSLog(@"findFilesInDirectory:%@:%@ failed: %@", directory, extension, [error description]);
        return [NSArray array];
    }
    
	NSMutableArray* returnValue = [[NSMutableArray alloc] init];
	for (NSString* file in files)
	{
		if ([[file pathExtension] isEqualToString:extension])
			[returnValue addObject:file];
	}
	
	return [NSArray arrayWithArray:returnValue];
}

@end
