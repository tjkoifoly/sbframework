//
//  NSFileManager+Directories.h
//  SBFramework
//
//  Created by Wim Haanstra on 6/15/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Directories)

// Gives you the full path to a directory with the name 'path' in the documents directory
// When the path does not exist, it creates the path
- (NSString*) directoryInDocumentsDirectory:(NSString*) path;

// Find files in a specified directory with a specific extension
- (NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString*) extension;

// Find files in a specified directory with a specific extension and the file name contains the supplied pattern
- (NSArray*) findFilesInDirectory:(NSString*) directory withExtension:(NSString *)extension andPattern:(NSString*) pattern;

// Remove files with a certain extension from the supplied path
- (NSUInteger) cleanDirectory:(NSString*) path filesWithExtension:(NSString*) extension;

@end
