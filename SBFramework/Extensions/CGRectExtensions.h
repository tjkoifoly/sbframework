//
//  CGRectExtensions.h
//  postercreator
//
//  Created by Wim Haanstra on 1/7/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum CGRectSide {
	CGRectSideTop,
	CGRectSideBottom,
	CGRectSideLeft,
	CGRectSideRight
} CGRectSide;

void CGRectLogWithDescription(NSString* description, CGRect rect);

void CGRectLog(CGRect rect);

CGPoint CGRectCenter(CGRect rect);

CGRect CGRectMoveToPoint(CGRect rect, CGPoint point);

CGRect CGRectCenterInRect(CGRect rect, CGSize size);

CGRect CGRectSwitchSides(CGRect rect);

void CGRectToRoundedCornersPath(CGContextRef context, CGRect rect, float cornerRadius);

UIBezierPath* CGRectCreateRoundedCornersPath(CGRect rect, float cornerRadius);

CGRect CGRectZeroLocation(CGRect rect);

CGPoint CGRectCenterOfSide(CGRect rect, CGRectSide side);