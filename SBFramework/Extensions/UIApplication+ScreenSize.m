//
//  UIApplication+ScreenSize.m
//  WorkPlace
//
//  Created by Wim Haanstra on 1/10/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import "UIApplication+ScreenSize.h"

@implementation UIApplication (ScreenSize)

- (CGSize) screenSize {
    UIInterfaceOrientation orientation = self.statusBarOrientation;
    CGSize size = [UIScreen mainScreen].bounds.size;

    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        size = CGSizeMake(size.height, size.width);
    }
    if (self.statusBarHidden == NO)
    {
        size.height -= MIN(self.statusBarFrame.size.width, self.statusBarFrame.size.height);
    }
	
    return size;	
}

- (UIView*) topView {
	
	UIWindow* topWindow = [[self.windows sortedArrayUsingComparator:^NSComparisonResult(UIWindow *win1, UIWindow *win2) {
        return win1.windowLevel < win2.windowLevel;
    }] lastObject];
	
    return [[topWindow subviews] lastObject];
}

@end
