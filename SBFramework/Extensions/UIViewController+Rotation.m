//
//  UIView (Rotation).m
//  SBFramework
//
//  Created by Wim Haanstra on 6/15/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//
//

#import "UIViewController+Rotation.h"

@implementation UIViewController (Rotation)

- (BOOL) shouldAllowRotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation
{
	NSString* currentOrientation = @"";
	switch (interfaceOrientation)
	{
		case UIInterfaceOrientationLandscapeLeft:
			currentOrientation = @"UIInterfaceOrientationLandscapeLeft";
			break;
		case UIInterfaceOrientationLandscapeRight:
			currentOrientation = @"UIInterfaceOrientationLandscapeRight";
			break;
		case UIInterfaceOrientationPortrait:
			currentOrientation = @"UIInterfaceOrientationPortrait";
			break;
		case UIInterfaceOrientationPortraitUpsideDown:
			currentOrientation = @"UIInterfaceOrientationPortraitUpsideDown";
			break;
	}
	
	NSString* supportedOrientationsKey = @"UISupportedInterfaceOrientations";
	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad &&
        [[[NSBundle mainBundle] infoDictionary] objectForKey:@"UISupportedInterfaceOrientations~ipad"] != nil)
    {
		supportedOrientationsKey = @"UISupportedInterfaceOrientations~ipad";
    }
    
	NSArray* allowedOrientations = [[[NSBundle mainBundle] infoDictionary] objectForKey:supportedOrientationsKey];
	for (NSString* allowedOrientation in allowedOrientations)
	{
		if ([allowedOrientation isEqualToString:currentOrientation])
			return YES;
	}
	
	return NO;
}

@end
