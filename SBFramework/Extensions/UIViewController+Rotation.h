//
//  UIView (Rotation).h
//  SBFramework
//
//  Created by Wim Haanstra on 6/15/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (Rotation)

- (BOOL) shouldAllowRotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation;

@end