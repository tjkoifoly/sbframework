//
//  SBHorizontalBar.m
//  GraphsTest
//
//  Created by Wim Haanstra on 5/26/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import "SBStackedHorizontalBar.h"

@interface SBStackedHorizontalBar (hidden)

- (CGRect) getValueRect:(NSUInteger) index;
- (CGFloat) totalValue;

@end

@implementation SBStackedHorizontalBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		animationChain = [[SBAnimationChain alloc] init];
		
        [self setMaximumValue:100];
        [self setTotalAnimationTime:1];
        [self setCornerRadius:12.0f];
        [self setInnerShadowWidth:4.0f];
        [self setAnimationCurve:UIViewAnimationCurveLinear];
		
		_barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [_barView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [_barView setTag:-100];
		[self addSubview:_barView];
		
		[[_barView layer] setMasksToBounds:YES];
		[[_barView layer] setCornerRadius:[self cornerRadius]];
		[[_barView layer] setBorderColor: [UIColor grayColor].CGColor];  //[UIColorFromRGB(180, 180, 180) CGColor]];
		[[_barView layer] setBorderWidth:1.0f];
		[[_barView layer] setShadowColor:[UIColor blackColor].CGColor];
		[[_barView layer] setShadowOffset:CGSizeMake(0, 0)];
		[[_barView layer] setShadowOpacity:1];
		[[_barView layer] setShadowRadius:[self innerShadowWidth]];
		
	}
    return self;
}

- (CGFloat) totalValue
{
	CGFloat returnValue = 0;
	
	for (int index = 0; index < [_dataSource numberOfItemsInBar:self]; index++)
		returnValue += [_dataSource valueForItem:index inBar:self];
	
	return returnValue;
}

- (void) setCornerRadius:(CGFloat) __cornerRadius
{
	_cornerRadius = __cornerRadius;
	[[_barView layer] setCornerRadius:[self cornerRadius]];
}

- (void) setInnerShadowWidth:(CGFloat) __innerShadowWidth
{
	_innerShadowWidth = __innerShadowWidth;
	[[_barView layer] setShadowRadius:[self innerShadowWidth]];
}

- (NSArray*) itemViews
{
	return [_barView subviews];
}

#pragma mark - Public methods

/* Redraw the graph, populating it again with the supplied values */
- (void) reloadDataWithAnimation:(BOOL) animated
{
	[animationChain clearAnimation];
	[self updateItems:animated];
}

#pragma mark - Touch detection


- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if ([touches count] == 0)
		return;
	
	if (![_delegate respondsToSelector:@selector(didSelectItemAtIndex:inBar:)])
		return;
	
	UITouch* touch = [touches anyObject];
	CGPoint touchPoint = [touch locationInView:_barView];
	
	for (UIView* itemView in [_barView subviews])
	{
		if (CGRectContainsPoint(itemView.frame, touchPoint))
		{
			[_delegate didSelectItemAtIndex:[itemView tag] inBar:self];
			break;
		}
	}
}

#pragma mark - Private methods

- (void) updateItems:(BOOL) animated
{
	[animationChain clearAnimation];
	
	if (animated)
        [animationChain setTotalAnimationTime:[self totalAnimationTime]];
	else
        [animationChain setTotalAnimationTime:0];
	
	NSUInteger numberOfItems = [_dataSource numberOfItemsInBar:self];
	
	if (numberOfItems < [[_barView subviews] count])
	{
		for (NSUInteger index = [[_barView subviews] count]; index >= numberOfItems; index--)
		{
			UIView* viewToRemove = [_barView viewWithTag:index];
			if (viewToRemove == nil)
				continue;
			
			[animationChain addAnimationBlock:^(void) {
				viewToRemove.frame = CGRectMake(viewToRemove.frame.origin.x, viewToRemove.frame.origin.y, 1, viewToRemove.frame.size.height);
			} 
								finishedBlock:^(void) 
			 {
				 [viewToRemove removeFromSuperview];
			 }
						   withAnimationCurve:UIViewAnimationCurveLinear];
		}
	}
		
	for (NSUInteger index = 0; index < numberOfItems; index++)
	{
		CGRect endFrame = [self getValueRect:index];
		UIView* v = [_barView viewWithTag:index];
		if (v == nil)
		{
			CGRect startFrame = CGRectMake(endFrame.origin.x, endFrame.origin.y, 0, endFrame.size.height);
			v = [[UIView alloc] initWithFrame:startFrame];
            [v setTag:index];

			UIColor* color = [_dataSource colorForItem:index inBar:self];
            [v setBackgroundColor:color];
			[_barView addSubview:v];
		}
		
		if (!CGRectEqualToRect(endFrame, v.frame))
		{
			[animationChain addAnimationBlock:^(void) {
                [v setFrame:endFrame];
			} finishedBlock:^(void) {} withAnimationCurve:UIViewAnimationCurveEaseInOut];
		}
	}
	
	[animationChain startAnimation];
}

- (CGRect) getValueRect:(NSUInteger) index
{
	CGFloat xPos = 0;
	
	for (NSUInteger i = 0; i < [_dataSource numberOfItemsInBar:self]; i++)
	{
		CGFloat value = [_dataSource valueForItem:i inBar:self];
		CGRect viewRect = CGRectMake(xPos, 0, 0, self.frame.size.height);
		
		if (value > 0)
		{
			CGFloat percentage = value / (self.maximumValue / 100);
			
			if (percentage > 0)
			{
				CGFloat width = self.frame.size.width * (percentage / 100);
				viewRect = CGRectMake(xPos, 0, width, self.frame.size.height);
				
				
				xPos += width;
			}
		}
		
		if (i == index)
		{
			return viewRect;
		}
	}
	
	return CGRectZero;
}



@end
