Stacked-Horizontal-Bar
======================

This is the source of the Stacked Horizontal Bar, published on my blog: http://www.sortedbits.com/stacked-progress-bar-graph/

![Alt text](http://www.sortedbits.com/wp-content/uploads/2012/05/GraphBar.png "The stacked horizontal bar")

Properties
----------

`@property (nonatomic, assign) id<NSObject, SBStackedHorizontalBarDelegate> delegate`  
The delegate which receives notifications from the bar  

`@property (nonatomic, assign) id<NSObject, SBStackedHorizontalBarDataSource> dataSource`  
The datasource for the bar  

`@property (nonatomic) CGFloat  				maximumValue`  
The maximum value for the graph to show, default value: 100  

`@property (nonatomic) CGFloat					totalAnimationTime`  
The time it should take to fill the bar with all values, used for animation, default value: 1.0f  

`@property (nonatomic) CGFloat					cornerRadius`  
The corner radius of the bar, not of the View it self, default value: 12.0f  

`@property (nonatomic) CGFloat					innerShadowWidth`  
The width of the inner shadow shown in the bar, default value: 4.0f  

`@property (nonatomic) UIViewAnimationCurve animationCurve`  
The animation curve used to draw each individual value in the bar, default value: UIViewAnimationCurveLinear

`@property (nonatomic, readonly) NSArray*		itemViews;`  
This array contains the views used to show the parts. This gives you access to the properties of each individual item

`@property (nonatomic, retain) UIView* barView;`  
The UIView where all the `itemViews` are placed upon, for easy access to layer properties

Datasource
----------
`- (NSInteger) numberOfItemsInBar:(SBHorizontalBar*) bar`  
This required method is called by the bar to receive the total number of 'parts' in your bar. This works just like the numberOfRowsInSection: method that is called by an UITableViewController. You need to return the number of parts you want to show.

`- (CGFloat) valueForItem:(NSInteger) itemNumber inBar:(SBHorizontalBar*) bar`  
This required method will ask your dataSource what value should be assigned to the part with index itemNumber. You will need to return a float value here.

`- (UIColor*) colorForItem:(NSInteger) itemNumber inBar:(SBHorizontalBar*) bar`  
This required method will ask you what color you would want to use for the part with index itemNumber. You can assign any UIColor here, so you could also work with UIImage objects (which I do in my example project).

Delegate
--------
`- (void) doneDrawing:(SBHorizontalBar*) bar`  
Optional: The method doneDrawing is called when the bar is done drawing all parts and all animation is done. It is optional, but it could be used to chain some things together (for example, showing a legend).

`- (void) didSelectItemAtIndex:(int) index inBar:(SBStackedHorizontalBar*) bar`  
Optional: The method didSelectItemAtIndex is called when a user touches one of the items in the bar.

Methods
-------
`- (void) reloadDataWithAnimation:(BOOL) animated`  
This reloads the bar with, it will ask the dataSource for any new values. The user can specify if it wants to animate the changes or that it wants to update without animations