//
//  SBHorizontalBar.h
//  GraphsTest
//
//  Created by Wim Haanstra on 5/26/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SBAnimationChain.h"

@class SBStackedHorizontalBar;

@protocol SBStackedHorizontalBarDelegate

@optional
- (void) doneDrawing:(SBStackedHorizontalBar*) bar;
- (void) didSelectItemAtIndex:(NSUInteger) index inBar:(SBStackedHorizontalBar*) bar;

@end

@protocol SBStackedHorizontalBarDataSource

- (NSInteger) numberOfItemsInBar:(SBStackedHorizontalBar*) bar;
- (UIColor*) colorForItem:(NSUInteger) itemNumber inBar:(SBStackedHorizontalBar*) bar;
- (CGFloat) valueForItem:(NSUInteger) itemNumber inBar:(SBStackedHorizontalBar*) bar;

@end

@interface SBStackedHorizontalBar : UIView
{
	SBAnimationChain* animationChain;
}

/* The delegate which receives notifications from the bar */
@property (nonatomic, assign) id<NSObject, SBStackedHorizontalBarDelegate> delegate;

/* The datasource for the bar */
@property (nonatomic, assign) id<NSObject, SBStackedHorizontalBarDataSource> dataSource;

/*
 The view used to place the items on. This view can be accessed to change properties on it.
*/
@property (nonatomic, retain) UIView* barView;

/* 
 The maximum value for the graph to show 
 Default value: 100;
*/
@property (nonatomic) CGFloat maximumValue;

/* 
 The time it should take to fill the bar with all values, used for animation 
 Default value: 1.0f
*/
@property (nonatomic) CGFloat totalAnimationTime;

/* 
 The corner radius of the bar, not of the View it self 
 Default value: 12.0f
*/
@property (nonatomic) CGFloat cornerRadius;

/* 
 The width of the inner shadow shown in the bar 
 Default value: 4.0f
*/
@property (nonatomic) CGFloat innerShadowWidth;

/* 
 The animation curve used to draw each individual value in the bar
 Default value: UIViewAnimationCurveLinear
*/
@property (nonatomic) UIViewAnimationCurve animationCurve;

/*
 The views used to show the items in the bar
*/
@property (nonatomic, readonly) NSArray* itemViews;

/* Redraw the graph, populating it again with the supplied values */
- (void) reloadDataWithAnimation:(BOOL) animated;

@end
