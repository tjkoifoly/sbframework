//
//  URLParser.h
//  SBFramework
//
//  Created by Wim Haanstra on 1/8/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLParser : NSObject

@property (nonatomic, readonly) NSURL* url;

@property (nonatomic, readonly) NSString* scheme;
@property (nonatomic, readonly) NSString* hostName;
@property (nonatomic, readonly) NSString* path;
@property (nonatomic, readonly) NSArray*  pathComponents;
@property (nonatomic, readonly) NSDictionary* parameters;
@property (nonatomic, readonly) NSString* tag;

+ (NSURLParser*) parserWithURL:(NSURL*) url;
- (id) initWithURL:(NSURL*) url;

@end
