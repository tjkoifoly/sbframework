//
//  URLParser.m
//  SBFramework
//
//  Created by Wim Haanstra on 1/8/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import "NSURLParser.h"

@implementation NSURLParser

+ (NSURLParser*) parserWithURL:(NSURL*) url {

	NSURLParser* parser = [[NSURLParser alloc] initWithURL:url];
	return parser;
	
}

- (id) initWithURL:(NSURL*) url {
	
	self = [self init];
	if (self) {
		_url = url;
		
		if (_url != nil)
			[self parseURL];
	}
	return self;
}

- (id) init {
	
	self = [super init];
	if (self) {
		_url = nil;
		_scheme = nil;
		_hostName = nil;
		_path = nil;
		
		_pathComponents = [NSArray new];
		_parameters = [NSDictionary new];
		
		_tag = nil;
	}
	return self;
}

- (void) parseURL {
	if (_url == nil)
		return;
	
	NSString* urlString = [[_url absoluteString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSArray* tagSplit = [urlString componentsSeparatedByString:@"#"];
	
	if ([tagSplit count] > 1) {
		_tag = [tagSplit objectAtIndex:1];
	}

	NSArray* parametersSplit = [[tagSplit objectAtIndex:0] componentsSeparatedByString:@"?"];
	
	if ([parametersSplit count] > 1) {
		NSArray* queryParameters = [[parametersSplit objectAtIndex:1] componentsSeparatedByString:@"&"];
		
		NSMutableDictionary* keyValueDictionary = [NSMutableDictionary new];
		
		for (NSString* queryParameter in queryParameters) {
			NSArray* keyValueArray = [queryParameter componentsSeparatedByString:@"="];
			if ([keyValueArray count] > 0) {
				NSString* key = [keyValueArray objectAtIndex:0];
				NSString* value = ([keyValueArray count] == 2) ? [keyValueArray lastObject] : nil;
				[keyValueDictionary setObject:value forKey:key];
			}
		}
		
		_parameters = [NSDictionary dictionaryWithDictionary:keyValueDictionary];
	}
	
	_path = [_url path];
	_pathComponents = [_url pathComponents];
	_scheme = [_url scheme];
	_hostName = [_url host];
}

- (NSString*) description {
	
	if (_url == nil)
		return @"<nil>";
	
	NSDictionary* dict = @{
		@"host" : _hostName,
		@"scheme": _scheme,
		@"path" : _path,
		@"pathComponents" : _pathComponents,
		@"parameters" : _parameters,
		@"tag" : _tag
	};
	
	return [NSString stringWithFormat:@"%@", dict];
}

@end
